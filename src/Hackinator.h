//
// Created by Jake on 4/29/2021.
//
#pragma once
#include <string>
using namespace std;

extern int AIR_GIVEN_WHEN_WEARING_AIR_TANK;
extern int AIR_GIVEN_WHEN_NOT_IN_WATER;
extern int AIR_GIVEN_WHEN_GAME_IS_LOADED;
extern int BOOSTER_0_8_FUEL;
extern int BOOSTER_2_0_FUEL;
extern int BOOSTER_0_8_STRENGTH;
extern int BOOSTER_2_0_STRENGTH;
extern int BOSS_HEALTH_OMEGA;
extern int BOSS_HEALTH_BALFROG;
extern int BOSS_HEALTH_MONSTER_X;
extern int BOSS_HEALTH_CORE;
extern int BOSS_HEALTH_IRONHEAD;
extern int BOSS_HEALTH_DRAGON_SISTERS;
extern int BOSS_HEALTH_UNDEAD_CORE;
extern int BOSS_HEALTH_HEAVY_PRESS;
extern int BOSS_HEALTH_BALLOS_P2;
extern int BOSS_HEALTH_BALLOS_P4;
extern int SPIKE_DAMAGE;
extern int STARTING_HEALTH_MAX;
extern int STARTING_HEALTH;
extern int STARTING_MAP;
extern int STARTING_EVENT;
extern int STARTING_POSITION_X;
extern int STARTING_POSITION_Y;
extern int STARTING_DIRECTION;
extern int TITLE_EVENT;
extern int TITLE_MAP;
extern int TOP_LEFT_FRAMERECT_X;
extern int TOP_LEFT_FRAMERECT_Y;
extern int BOTTOM_RIGHT_FRAMERECT_X;
extern int BOTTOM_RIGHT_FRAMERECT_Y;
extern int TITLE_SCREEN_SONG;
extern int ENABLE_CION_HUD;
extern int CION_REPLACES_EXP;
extern int CION_DROP_CHANCE;
extern int APPROXIMATE_STAGE_LIGHTING;
extern int ZX_SKIPS_NOD;

// Store 2 arrays, one for storing where a key is in the second array
// unordered map didnt compile, so uhhhh idk I dont know what im doing
#define HACKINATOR_MAX 100
extern string lookup_map[HACKINATOR_MAX];
extern int* value_map[HACKINATOR_MAX];

void InitializeHackinator();
