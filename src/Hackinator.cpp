//
// Created by Jake on 4/29/2021.
//
#include "Hackinator.h"
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>

int AIR_GIVEN_WHEN_WEARING_AIR_TANK = 1000; //IMP
int AIR_GIVEN_WHEN_NOT_IN_WATER = 1000; //IMP
int AIR_GIVEN_WHEN_GAME_IS_LOADED = 1000; //IMO
int BOOSTER_0_8_FUEL = 50; //IMP
int BOOSTER_2_0_FUEL = 50; //IMP
int BOOSTER_0_8_STRENGTH = 1535;
int BOOSTER_2_0_STRENGTH = 1535; //IMP
int BOSS_HEALTH_OMEGA = 400;
int BOSS_HEALTH_BALFROG = 300;
int BOSS_HEALTH_MONSTER_X = 700;
int BOSS_HEALTH_CORE = 650;
int BOSS_HEALTH_IRONHEAD = 400;
int BOSS_HEALTH_DRAGON_SISTERS = 500;
int BOSS_HEALTH_UNDEAD_CORE = 700;
int BOSS_HEALTH_HEAVY_PRESS = 700;
int BOSS_HEALTH_BALLOS_P2 = 800;
int BOSS_HEALTH_BALLOS_P4 = 1200;
int SPIKE_DAMAGE = 10; //IMP
int STARTING_HEALTH_MAX = 3;
int STARTING_HEALTH = 3;
int STARTING_MAP = 13;
int STARTING_EVENT = 200;
int STARTING_POSITION_X = 10;
int STARTING_POSITION_Y = 8;
int STARTING_DIRECTION = 2;
int TITLE_EVENT = 100;
int TITLE_MAP = 72;
int TOP_LEFT_FRAMERECT_X = 0;
int TOP_LEFT_FRAMERECT_Y = 0;
int BOTTOM_RIGHT_FRAMERECT_X = 144;
int BOTTOM_RIGHT_FRAMERECT_Y = 40;
int TITLE_SCREEN_SONG = 24;
int ENABLE_CION_HUD = 0;
int CION_REPLACES_EXP = 0;
int CION_DROP_CHANCE = 0;
int APPROXIMATE_STAGE_LIGHTING = 0;
int ZX_SKIPS_NOD = 0;

string lookup_map[HACKINATOR_MAX];
int* value_map[HACKINATOR_MAX];

void PutHackinatorValue(const string& key, int* value){
    for (int i = 0; i < HACKINATOR_MAX; ++i) {
        if(lookup_map[i].empty()){
            lookup_map[i] = key;
            value_map[i] = value;
            return;
        }
    }
}

int* GetHackinatorPointer(const string& key){
    for (int i = 0; i < HACKINATOR_MAX; ++i) {
        if(lookup_map[i] == key){
            return value_map[i];
        }
    }
    return NULL;
}
void ClearHackinator(){
    for (int i = 0; i < HACKINATOR_MAX; ++i) {
        lookup_map[i] = "";
        value_map[i] = NULL;
    }
}
void CreateHackinatorTable(){
    PutHackinatorValue("AIR_GIVEN_WHEN_WEARING_AIR_TANK", &AIR_GIVEN_WHEN_WEARING_AIR_TANK);
    PutHackinatorValue("AIR_GIVEN_WHEN_NOT_IN_WATER", &AIR_GIVEN_WHEN_NOT_IN_WATER );
    PutHackinatorValue("AIR_GIVEN_WHEN_GAME_IS_LOADED", &AIR_GIVEN_WHEN_GAME_IS_LOADED );
    PutHackinatorValue("BOOSTER_0_8_FUEL", &BOOSTER_0_8_FUEL );
    PutHackinatorValue("BOOSTER_2_0_FUEL", &BOOSTER_2_0_FUEL );
    PutHackinatorValue("BOOSTER_0_8_STRENGTH", &BOOSTER_0_8_STRENGTH );
    PutHackinatorValue("BOOSTER_2_0_STRENGTH", &BOOSTER_2_0_STRENGTH );
    PutHackinatorValue("BOSS_HEALTH_OMEGA", &BOSS_HEALTH_OMEGA );
    PutHackinatorValue("BOSS_HEALTH_BALFROG", &BOSS_HEALTH_BALFROG );
    PutHackinatorValue("BOSS_HEALTH_MONSTER_X", &BOSS_HEALTH_MONSTER_X );
    PutHackinatorValue("BOSS_HEALTH_CORE", &BOSS_HEALTH_CORE );
    PutHackinatorValue("BOSS_HEALTH_IRONHEAD", &BOSS_HEALTH_IRONHEAD );
    PutHackinatorValue("BOSS_HEALTH_DRAGON_SISTERS", &BOSS_HEALTH_DRAGON_SISTERS );
    PutHackinatorValue("BOSS_HEALTH_UNDEAD_CORE", &BOSS_HEALTH_UNDEAD_CORE );
    PutHackinatorValue("BOSS_HEALTH_HEAVY_PRESS", &BOSS_HEALTH_HEAVY_PRESS );
    PutHackinatorValue("BOSS_HEALTH_BALLOS_P2", &BOSS_HEALTH_BALLOS_P2 );
    PutHackinatorValue("BOSS_HEALTH_BALLOS_P4", &BOSS_HEALTH_BALLOS_P4 );
    PutHackinatorValue("SPIKE_DAMAGE", &SPIKE_DAMAGE );
    PutHackinatorValue("STARTING_HEALTH_MAX", &STARTING_HEALTH_MAX );
    PutHackinatorValue("STARTING_HEALTH", &STARTING_HEALTH );
    PutHackinatorValue("STARTING_MAP", &STARTING_MAP );
    PutHackinatorValue("STARTING_EVENT", &STARTING_EVENT );
    PutHackinatorValue("STARTING_POSITION_X", &STARTING_POSITION_X );
    PutHackinatorValue("STARTING_POSITION_Y", &STARTING_POSITION_Y );
    PutHackinatorValue("STARTING_DIRECTION", &STARTING_DIRECTION );
    PutHackinatorValue("TITLE_EVENT", &TITLE_EVENT );
    PutHackinatorValue("TITLE_MAP", &TITLE_MAP );
    PutHackinatorValue("TOP_LEFT_FRAMERECT_X", &TOP_LEFT_FRAMERECT_X );
    PutHackinatorValue("TOP_LEFT_FRAMERECT_Y", &TOP_LEFT_FRAMERECT_Y );
    PutHackinatorValue("BOTTOM_RIGHT_FRAMERECT_X", &BOTTOM_RIGHT_FRAMERECT_X );
    PutHackinatorValue("BOTTOM_RIGHT_FRAMERECT_Y", &BOTTOM_RIGHT_FRAMERECT_Y );
    PutHackinatorValue("TITLE_SCREEN_SONG", &TITLE_SCREEN_SONG );
    PutHackinatorValue("ENABLE_CION_HUD", &ENABLE_CION_HUD );
    PutHackinatorValue("CION_REPLACES_EXP", &CION_REPLACES_EXP );
    PutHackinatorValue("CION_DROP_CHANCE", &CION_DROP_CHANCE );
    PutHackinatorValue("APPROXIMATE_STAGE_LIGHTING", &APPROXIMATE_STAGE_LIGHTING );
    PutHackinatorValue("ZX_SKIPS_NOD", &ZX_SKIPS_NOD );
}
void LoadHackinatorFromFile(){
    std::ifstream infile("data/hackinator.txt");
    std::string line;
    while (std::getline(infile, line)) {
        char c = line[0];
        if(line[0] != '#' && line[0] != '\0'){
            std::string key = line.substr(0, line.find(" = "));
            std::string value = line.substr(line.find(" = ") + 3, line.length());
            int i_value = atoi(value.c_str());
            int* ptr = GetHackinatorPointer(key);
            *ptr = i_value;
        }
    }
}
void InitializeHackinator(){
    ClearHackinator();
    CreateHackinatorTable();
    LoadHackinatorFromFile();
}

