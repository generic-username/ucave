//
// Created by Jake on 4/25/2021.
//

#pragma once
int hashTSC(char a,char b,char c){
    return a + (b << 8) + (c << 16);
}

typedef void (*TSCFUNCTION)();
extern const TSCFUNCTION gTscFunctionTable[];

void TSC_END(){

}
